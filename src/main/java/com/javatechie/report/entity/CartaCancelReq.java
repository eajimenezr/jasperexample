package com.javatechie.report.entity;


import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

@Getter
@Setter
public class CartaCancelReq implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cve_folio;
	private String num_tdc;
	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tdc() {
		return num_tdc;
	}
	public void setNum_tdc(String num_tdc) {
		this.num_tdc = num_tdc;
	}

	
	
	
}
